package com.apm.apm.test;

import com.apm.apm.test.model.GetAllTestResponse;
import com.apm.apm.test.model.GetTestResponse;
import com.apm.apm.test.model.SaveTestRequest;
import com.apm.apm.test.model.UpdateTestRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@Slf4j
public class TestServiceImpl implements TestService {

    private final TestMapper testMapper;

    private final TestRepository testRepository;

    public TestServiceImpl(TestMapper testMapper, TestRepository testRepository) {
        this.testMapper = testMapper;
        this.testRepository = testRepository;
    }

    @Override
    public GetAllTestResponse getAll() {
        return new GetAllTestResponse(testRepository.findAll().stream().map(testMapper::toGetResponse).collect(Collectors.toList()));
    }

    @Override
    public GetTestResponse get(String id) {
        return testMapper.toGetResponse(testRepository.findById(id).orElseThrow(() -> new TestNotFoundException("Test with Id: " + id + " not found")));
    }

    @Override
    public GetTestResponse save(SaveTestRequest request) {
        return testMapper.toGetResponse(testRepository.save(testMapper.toEntity(request)));
    }

    @Override
    public GetTestResponse update(UpdateTestRequest request) {
        return testMapper.toGetResponse(testRepository.save(testMapper.toEntity(request)));
    }

    @Override
    public void delete(String id) {
        testRepository.findById(id).ifPresent(testRepository::delete);
    }
}
