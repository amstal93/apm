package com.apm.apm.test.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class GetTestResponse extends TestBaseModel {

    private String id;
}
