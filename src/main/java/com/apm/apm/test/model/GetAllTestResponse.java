package com.apm.apm.test.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class GetAllTestResponse {

    private List<GetTestResponse> testList;
}
